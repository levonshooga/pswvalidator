import App from "../../App";

function StrengthLevel(props) {
    return (
        <div>
            <span className={props.class}>Password reliability <span>({props.score} out of 4)</span></span>
        </div>
    );
}

export default StrengthLevel;