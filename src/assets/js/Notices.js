import App from "../../App";

function Warning(props) {
    let warning = props.warning;
    if( typeof warning !== "undefined" && warning !== '' ) {
        return (
            <div className={props.warningClassName}>
                <i>{props.warning}</i>
            </div>
        )
    }else {
        return '';
    }
}

function Suggestions(props) {
    let suggestions = props.suggestions;
    if( typeof suggestions !== "undefined" && suggestions.length !== 0 && suggestions !== '' ) {
        return (
            <div id="suggestions" className={props.suggestionsClassName}>
                <i>{suggestions.join(' ')}</i>
            </div>
        );
    }else {
        return '';
    }
}

function Notices(props) {
    let notices = props.notices;
    let warningClassName = props.warningClassName;
    let suggestionsClassName = props.suggestionsClassName;
    return (
        <div>
            <Warning warning={notices.warning} warningClassName={warningClassName} />
            <Suggestions suggestions={notices.suggestions} suggestionsClassName={suggestionsClassName} />
        </div>
    );
}

export default Notices;