import './App.css';
import { useState } from 'react';
import zxcvbn from 'zxcvbn';
import StrengthBar from './assets/js/StrengthBar';
import StrengthLevel from './assets/js/StrengthLevel';
import Notices from './assets/js/Notices';

function PswData(value) {
  return zxcvbn(value);
}

function App() {
  const [score, setScore] = useState(0);
  const [notices, setNotices] = useState('');
  
  return (
    <div className="App">
      <label>
        Password:<br/>
        <input type="password" onChange={(evt) => {
          let psw = PswData(evt.target.value);
          setScore(psw.score);
          setNotices(psw.feedback);
        }} />
      </label>

      <StrengthLevel score={score} class="strength-level" />

      <StrengthBar score={score} class="strength-bar" />

      <Notices notices={notices} warningClassName="notices warning" suggestionsClassName="notices suggestions" />
    </div>
  );
}

export default App;
